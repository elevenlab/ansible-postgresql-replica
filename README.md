Ansible Role: PostgreSQL-replica
===================
Install and configure PostgreSQL allowing master-slave configuration on Debian servers

It is divided in three sub-roles:

 * pgCommon
 * pgMaster
 * pgSlave

*pgCommon* contains the tasks common to both *pgMaster* and *pgSlave* sub-roles: it will install the packages required and configure postgreSQL conf file.

*pgMaster* provides operation that must be executed only on the master (which has write capabilities on the cluster): it will set the admin password, creates the user and the slot required for replication.

*pgSlave* provide the operation needed to the replica server to be attached to the master:  it will remove the data directory (if cluster is not yet configured), starts the backup procedure form the master, creates the `recovery.conf` file and starts the replication system.

----------
Requirements:
----------
Using an *automatic failover* system like [pacemaker](https://github.com/ClusterLabs/pacemaker), it is **strongly** advised to prepare the playbook which uses these role in a different way from the standard.

You have to differentiate the first setup from other executions of the playbook. Executing the *pgMaster* role on a server already configured can have multiple issue:

* You can't have the guarantee that the node you think is the master is actually the master, so executing the role on the wrong node has an unpredictable behavior. (And will **bring to a data-loss**)
* the *pgMaster* role will restart the master's PostgreSQL process, so the failover system will react as a master fail occurred, switching your master down and promoting a slave node.

Executing *pgSlave* on a master node can have even more issue: It will set you master server as Replica, completely destroying its data directory. bring to **potentially data loss** (e.g. if no other replica have the database updated).



Role variables
-------------
Available variables are listed below, along with default values (see `defaults/main.yml`)

The following variables are common to all the three sub-roles:

	pg_replication_user: replica
	pg_version: '9.6'
	pg_cluster_name: 'main'
	pg_port: 5432
	pg_ctl: service
The `pg_version` defaults to `9.6` but for Debian 8 `9.4` is needed. The `pg_replication_user` define the user which is responsible to replicate data from master to slave. (the user will run on slave and try to connect to master). `pg_cluster_name` is the name of the *pgsql* cluster. With the variable`pg_ctl` you can set if the [`start`, `restart`,`stop` and `reload`] operations inside the role are executed through the *service* command (value: `service`, default) or through *pg_ctlcluster* command (value: `cluster`).

When running with *pacemaker*, `pg_ctl: cluster` is **recommended** because internally *pacemkaer* itself use the *pg_ctl* command (*pg_ctlcluster* is an abstraction to *pg_ctl*) and it can have problem controlling postgreSQL if commands have been issued with *service*.

*pgCommon* also has these variable:

	pg_max_wal_senders: 5
	pg_max_replication_slot: 3
these allow to specify how many replica server you can connect (**manually** *restart is required to apply changes*). `pg_max_replication_slot` is the exact number of slot allowed to be allocated and `pg_max_wal_senders` specifies the maximum number of concurrent connections from standby servers (i.e., the maximum number of simultaneously running WAL sender processes).
>  [`pg_max_wal_senders`] should be set slightly higher than the maximum number of expected clients so disconnected clients can immediately reconnect.

	pg_shared_buffers: 256MB

As reported in the postgreSQLl [documentation](https://www.postgresql.org/docs/9.6/static/runtime-config-resource.html#GUC-SHARED-BUFFERS):
>Sets the amount of memory the database server uses for shared memory buffers. The default is typically 128 megabytes (128MB), but might be less if your kernel settings will not support it (as determined during initdb). This setting must be at least 128 kilobytes. (Non-default values of BLCKSZ change the minimum.) However, settings significantly higher than the minimum are usually needed for good performance. This parameter can only be set at server start.

>If you have a dedicated database server with 1GB or more of RAM, a reasonable starting value for shared_buffers is 25% of the memory in your system. There are some workloads where even large settings for shared_buffers are effective, but because PostgreSQL also relies on the operating system cache, it is unlikely that an allocation of more than 40% of RAM to shared_buffers will work better than a smaller amount. Larger settings for shared_buffers usually require a corresponding increase in checkpoint_segments, in order to spread out the process of writing large quantities of new or changed data over a longer period of time.

>On systems with less than 1GB of RAM, a smaller percentage of RAM is appropriate, so as to leave adequate space for the operating system. Also, on Windows, large values for shared_buffers aren't as effective. You may find better results keeping the setting relatively low and using the operating system cache more instead. The useful range for shared_buffers on Windows systems is generally from 64MB to 512MB.

	pg_synchronous_commit: 'off'
This variable enable the [synchronous replication](https://www.postgresql.org/docs/9.2/static/warm-standby.html#SYNCHRONOUS-REPLICATION)

	pg_restart_after_crash: 'on'
If something went wrong as default the postgreSQL server will restart automatically. It is **strongly** recommended to set it to `off` when the cluster is controlled by *pacemaker* or any other *automatic failover* system.

	pg_accept_connection: []
A list of connections that the server should allow. Each element has the following required fields (You don't need to specify here the replica server to allow access to master, they have their own specification `pg_replicas` which is required also for replica slot):
	
	hosts: []
	database:
	user:
	mask:
	type:
When the hosts provided are in the form of ip addresses you must specify the `mask` in the [CIDR](https://tools.ietf.org/html/rfc4632) notation: e.g:`/32` means the single host. If the host are provided in the form of hostname that the server can resolve (typically FQDN), you must set `mask: ''`.  `type` is the type of auth: typically `md5` is the expected value.
for further details look the postgreSQL documentation of [`pg_hba`](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html)

	pg_replicas: []
This list is common to both *pgCommon* and *pgMaster*.
It is the list of hosts that should be set as replica. they **MUST** be provided in the form of hostname that the server can resolve. (ip addresses are **not supported**).

The following variables are common to `pgMaster` and `pgSlave`:

	pg_replication_slot_name: 'slot'
This specifies the prefix name of the replication slot. Name will be `pg_replication_slot_name`+`hostname | regex_replace('[-.]', '_')` which avoids invalid slot name.
	
    pg_enabled: true
This specifies if the database should be enabled to start at boot. It is **strongly** recommended to set it to `false` when the cluster is controlled by *pacemaker* or any other *automatic failover* system.

----

The last variable is for *pgSlave* role only:
	
	pg_master_port: 5432
which specifies the port of the master postgreSQLserver the slave has to connect to.


   
Dependencies
-------------------
None.

Example Playbook
--------------------------

Consider this as **first install only** playbook:

    - name: install and configure master db
      hosts: dbmaster
      vars: 
        pg_replicas: "{{ groups['database'] | difference(groups['dbmaster']) }}"

      roles:
        - postgresql-replica/pgCommon
        - postgresql-replica/pgMaster
        
    - name: install and configure slave db
      hosts: dbslave
      vars: 
        pg_master: "{{ vip_master }}"
        pg_replicas: "{{ groups['database'] }}"
        pg_master_port: "hostvars[groups['dbmaster'][0]]['pg_port']}}"
     
      roles:
        - postgresql-replica/pgCommon
        - postgresql-replica/pgSlave
        
this as `inventory`:

    [dbmaster]
    serv1

    [dbslave]
    serv2
    serv3

    [database:children]
    dbmaster
    dbslave

And these as variables in `group_vars/all`:

    pg_ctl: cluster
    pg_version: '9.4'
    pg_cluster_name: 'main'
    pg_port: 5432
    pg_replication_slot_name: 'rep'
    pg_restart_after_crash: 'off'
    pg_synchronous_commit: 'on'
    pg_max_wal_senders: 5
    pg_max_replication_slot: 3
    vip_master: 10.0.0.100
